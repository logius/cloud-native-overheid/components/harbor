# Harbor

Links:
- Chart sources and flags: https://github.com/goharbor/harbor-helm
- Releases: https://github.com/goharbor/harbor-helm/releases
- Docs: https://goharbor.io/docs

## Active and Standby in a multi DC setup.

The Ansible role for Harbor has features for backup and failover between an active and a standby site. The supported architecture is a setup with two sites, where only one site is active, the other site is standby. The images are stored in S3.
There are two main scenarios:
- Planned Migration: the active site is put in maintenance mode, the passive site becomes active. In this proces, the data is transfered from active to passive with backups. Note the backup only includes database and K8s objects, container images are not part of the backup. In the planned migration the external ingress is removed to disconnect a site from the loadbalancer. The passive site is activated by enabling the ingress and restoring the data.
- Disaster Recovery: the active site goes down. The standby site will be promoted to active. The last available backup is restored.

These scenarios have the following preconditions:
- Images on the active site and the standby site are typically pulled via a Loadbalancer. The benefit is that the image URL is always the same.
- The active site and the standby site connect to the same S3 backend. This make the switch between the sites easy.
- The S3 site may have failover to another site. The DR of the images is not a concern for the Harbor DR.

In the tasklist recovery/recovery_jobs are multiple tags provided to support these scenarios.

For a DR recovery plan, there must also be a procedure to make sure there will never be two active sites. This exercise is not handled by the ansible role but must be handled by the admin user or by the Load balancer.

## Ingress

There are two ingresses for the Harbor webservice: 'external ingress' and 'admin ingress'. The external ingress is for end users and will typically be connected to the global loadbalancer.
The admin ingress is for connecting the Harbor products and for the admin user aiming to connect to Harbor without using the global loadbalancer. The admin ingress is specified with `config.harbor.fqdn`, the external ingress with `config.harbor.external_access.name`. If the external ingress is not specified, it will default to the admin ingress.

## Solving github trivy DB rate limiting

Default API rate-limit for github is 60/hr, increases to 5000/hr when a token is included. Trivy can be presented with a token through the harbor chart. It can be optionally included by setting the env variable __GITHUB_API_TOKEN__ in the container running the ansible code for deploying the application.

## Solving dockerhub rate limiting

Since the 1st of November 2020 dockerhub limits image pulls (anonymous and free accounts) to 5.000 pulls / 6 hours. The final rate limits are: Anonymous free users will be limited to 100 pulls per six hours, and authenticated free users will be limited to 200 pulls per six hours. The rate limits will be gradually implemented.
- https://www.docker.com/blog/docker-hub-image-retention-policy-delayed-and-subscription-updates/
- https://www.docker.com/increase-rate-limits

There are several processes in which images are pulled from dockerhub. Each process has a different solution for making less pulls from dockerhub.

__1. Building docker images__

- From a Gitlab pipeline within the platform the base images could be pulled via a docker hub proxy. Developers can work with build args in Kaniko, e.g. `FROM $docker_hub/library/python:3-slim`. Where docker_hub is a build var, by default points to docker.io.
- A replication can be configured by the platform team. Drawbacks are
    - Maintenance burden for the platform team.
    - Storage capacity needed for a lot of repositories

__2. Helm deployment__

- If the values in the the chart provides a setting for specifying a registry, provide the proxy url.

__3. Operator deployments__

- Most of the times a Custom Resource needs to be created. If the Custom Resource provides a way to specify the registry url provide the url of the proxy.

__4. Direct deployments (f.e. k8s Jobs)__

- Use the proxy in the image name.

## Auth with OIDC

#### Keycloak

There is an option to setup OIDC for harbor using Keycloak, which is also the default configured setup. This sets up a new client for the application in Keycloak and then uses that client for the setup in harbor.

The following keys are required for the registration of a new client in __Keycloak__:
* `config.system.cluster_name`
* `config.keycloak.external_access.fqdn`
* `config.keycloak.realm`
* `config.harbor.external_access.fqdn`
* `config.harbor.fqdn`

The config requirements for the registration in Harbor can be found in `/templates/oidc_config.json`. Note that the code path for setting up keycloak will use a value for __oidc_user_claim__ of __email__ to automatically set the usernames in Harbor to the email registered in Keycloak.

#### Keyhub

There is an option to setup OIDC for harbor using Keyhub. This is not the default setting, this codepath requires the setting of __config.system.identity_provider__ to the value __keyhub__.

The following keys are required for the registration of a new client in __Keyhub__:
* `config.system.identity_provider_url`
* `config.harbor.fqdn`

Additionally, the host machine must have the following ENV set:
* `KEYHUB_OIDC_HARBOR_CLIENT_ID`
* `KEYHUB_OIDC_HARBOR_CLIENT_SECRET`

The config requirements for the registration in Harbor can be found in `/templates/oidc_config.json`. When using Keyhub the Harbor username will be set to the __name__ attribute provided by the IDP in the IDtoken.
