# 
#  Create a public facing Ingress. This ingress will be connected with the Loadbalancer. 
#  The public facing Ingress is used side by side with the default Ingress 
#  which will be used for internal connections, as well as data center testing without load balancer. 
# 

- name: External Ingress 
  block: 
    - name: Get default Ingress for the settings maintained by helm chart
      k8s_info: 
        kind: Ingress
        api_version: networking.k8s.io/v1beta1
        name: "{{ config.harbor.fullname_override }}-ingress"
        namespace: "{{ config.harbor.namespace }}"
      register: ingress

      # Do not hardcode the http rules as they are fairly complex, instead copy from the default ingress 
    - name: Copy default http rules  
      set_fact: 
        default_http_rules: "{{ ingress.resources[0].spec.rules[0].http }}"

      # Do not hardcode annotations, instead copy from default 
    - name: Copy default annotations
      set_fact: 
        default_annotations: "{{ default_annotations | default({}) | combine({ annotation.key : annotation.value }) }}"
      when: annotation.key is not match('meta.helm.sh.*')
      loop_control: 
        loop_var: annotation
      loop: "{{ ingress.resources[0].metadata.annotations | dict2items }}"

    - name: patch whitelist annotation
      set_fact: 
        default_annotations: "{{ default_annotations | default({}) | combine({ 'nginx.ingress.kubernetes.io/whitelist-source-range' : config.harbor.whitelist_source_range | default(omit) }) }}"

    - name: Create external ingress 
      k8s: 
        state: present
        force: true # make sure removals from annotations are reflected
        definition: 
          apiVersion: networking.k8s.io/v1beta1
          kind: Ingress
          metadata:
            name: "{{ name }}" 
            namespace: "{{ config.harbor.namespace }}"
            annotations: "{{ default_annotations }}"
          spec:
            rules:
            - host: "{{ fqdn }}" 
              http: "{{ default_http_rules }}"
            tls:
            - hosts:
              - "{{ fqdn }}" 
              secretName: "{{ tls_secret_name }}" 
      vars:
        name: "{{ config.harbor.external_access.name }}"
        fqdn: "{{ config.harbor.external_access.fqdn }}"
        tls_secret_name: "{{ config.harbor.external_access.tls_secret_name }}"
  when: 
    - config.harbor.external_access.name is defined
    - lookup('env','ACTIVE_SITE') | length == 0 or lookup('env','ACTIVE_SITE') == config.system.cluster_name

- name: Secondary Ingress
  block:
    - name: Get default Ingress for the settings maintained by helm chart
      k8s_info:
        kind: Ingress
        api_version: networking.k8s.io/v1beta1
        name: "{{ config.harbor.fullname_override }}-ingress"
        namespace: "{{ config.harbor.namespace }}"
      register: ingress

      # Do not hardcode the http rules as they are fairly complex, instead copy from the default ingress
    - name: Copy default http rules
      set_fact:
        default_http_rules: "{{ ingress.resources[0].spec.rules[0].http }}"

    - name: Create secondary ingress
      k8s:
        state: present
        force: true # make sure removals from annotations are reflected
        definition:
          apiVersion: networking.k8s.io/v1beta1
          kind: Ingress
          metadata:
            name: "{{ config.harbor.fullname_override }}-secondary-ingress"
            namespace: "{{ config.harbor.namespace }}"
            annotations: "{{ ingress.resources[0].metadata.annotations }}"
          spec:
            rules:
            - host: "{{ config.harbor.secondary_fqdn }}"
              http: "{{ default_http_rules }}"
            tls:
            - hosts:
              - "{{ config.harbor.secondary_fqdn }}"
              secretName: "{{ config.harbor.tls_secret_name }}-secondary"
  when: 
    - config.harbor.secondary_fqdn  is defined
